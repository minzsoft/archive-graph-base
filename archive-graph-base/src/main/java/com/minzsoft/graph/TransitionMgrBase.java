package com.minzsoft.graph;

import com.minzsoft.bean.BaseBean;
import java.util.Map;
import java.util.HashMap;
import javax.validation.constraints.NotNull;
import com.minzsoft.cache.BeanGuardCache;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * manager class to set instance of Transition workable
 *
 * @author richard
 */
@Setter
@Getter
@Accessors(chain = true, fluent = true)
public abstract class TransitionMgrBase {

    @Autowired
    private Transition transition = null;

    @Autowired
    private BeanGuardCache beanGuardCache = null;

    /**
     * default contructor
     */
    public TransitionMgrBase() {
        super();
    }

    /**
     * create session
     *
     * @return
     * @throws java.lang.Exception
     */
    @NotNull
    public abstract Map<String, Object> createSession()
            throws Exception;

    /**
     * create map with auto beans
     *
     * @return
     * @throws java.lang.Exception
     */
    @NotNull
    protected Map<String, Object> createAutoBeans()
            throws Exception {

        final Map<String, Object> result = new HashMap<>();

        if ((transition == null) || (beanGuardCache == null)) {
            throw new RuntimeException(
                    "TransitionMgrBase not correct initialized!");
        }

        transition
                .classnames()
                .forEach(
                        (var classname) -> {
                            try {

                                final BaseBean bean = (BaseBean) Class
                                        .forName(classname)
                                        .getConstructor()
                                        .newInstance();
                                bean
                                        .setGuard(
                                                beanGuardCache
                                                        .get(
                                                                bean.getBeanId()));
                                result
                                        .put(
                                                bean.getBeanId(), bean);

                            } catch (Exception ex) {

                                throw new RuntimeException(
                                        ex);
                            }
                        });

        return result;
    }
}