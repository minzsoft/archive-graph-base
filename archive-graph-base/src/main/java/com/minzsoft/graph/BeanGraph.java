/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minzsoft.graph;

import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author richard
 */
public interface BeanGraph {

    /**
     * add vertexe
     *
     * @param vertexe
     * @return
     */
    public BeanGraph addVertex(@NotEmpty final String vertexe);

    /**
     * add list of vertexes
     *
     * @param vertexes
     * @return
     */
    public BeanGraph addVertexes(@NotNull final List<String> vertexes);

    /**
     * add edge
     *
     * @param trigger
     * @param clause
     * @return
     */
    public BeanGraph addEdge(@NotNull final String trigger,
            @NotNull final String clause);

    /**
     * create direct acycle graph
     *
     * @return
     */
    public BeanGraph createGraph();

    /**
     * get whole transition
     *
     * @return
     */
    @NotNull
    public List<String> transition();

    /**
     * get transition beginning with vertexe
     *
     * @param vertexe
     * @return
     */
    @NotNull
    public List<String> transition(@NotEmpty final String vertexe);

    /**
     * get sources of edges target to vertexe
     *
     * @param vertexe
     * @return
     */
    @NotNull
    public Set<String> incomingVertexesOf(@NotEmpty final String vertexe);

    /**
     * setter
     *
     * @param transitionTail
     * @return
     */
    public BeanGraphImpl transitionTail(@NotNull List<String> transitionTail);

    /**
     * getter
     *
     * @return
     */
    public List<String> transitionTail();

    /**
     * getter
     *
     * @return
     */
    public Set<String> vertexes();
}
