package com.minzsoft.graph;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Die Klasse beschreibt die Abhängigkeiten von ViewBeans mit den Mitteln der
 * Graphentheorie. Dabei wird eine Linearisierung durchgeführt, bei der die
 * links von einem Elemente a stehenden Elemente unabhängig von a sind
 *
 * @author richard
 */
public interface Transition {

    /**
     * add edge
     *
     * @param source
     * @param target
     * @return
     */
    public Transition addEdge(@NotEmpty final String source, @NotEmpty final String target);

    /**
     * add vertexe
     *
     * @param vertexe
     * @return
     */
    public Transition addVertex(@NotEmpty final String vertexe);

    /**
     * add vertexe
     *
     * @param vertexes
     * @return
     */
    public Transition addVertexes(@NotNull final List<String> vertexes);

    /**
     * create transition
     *
     * @return
     */
    public Transition createGraph();

    /**
     * traverse transistion, starting with start
     *
     * @param start
     * @param model
     * @return
     */
    public int traverse(@NotEmpty final String start,
            @NotNull final Map<String, Object> model);

    /**
     * traverse whole transistion
     *
     * @param model
     * @return
     */
    public int traverse(@NotNull final Map<String, Object> model);

    /**
     *
     * @return
     */
    public List<String> transition();

    /**
     * add vertexe
     *
     * @param vertexes
     * @return
     */
    public Transition transitionTail(@NotNull final List<String> vertexes);

    /**
     * get transitionTail
     *
     * @return
     */
    public List<String> transitionTail();

    /**
     * get classname of beans
     *
     * @return
     */
    public Collection<String> classnames();

    /**
     * get set of vertexes
     *
     * @return
     */
    public Set<String> vertexes();
}
