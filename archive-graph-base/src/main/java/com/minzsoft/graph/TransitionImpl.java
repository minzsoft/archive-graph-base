package com.minzsoft.graph;

import com.minzsoft.bean.BaseBean;
import java.util.Map;
import com.minzsoft.guard.BeanGuard;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import com.minzsoft.annotation.MagellanSessionBean;
import static com.minzsoft.utils.AnnotationUtils.getAnnotatedClasses;
import static com.minzsoft.utils.LogUtils.getApplicationLogger;

/**
 *
 * @author richard
 */
@Component
@ToString(exclude = {"logger"})
@Accessors(chain = true, fluent = true)
@PropertySource(value = {"classpath:Graph.properties"})
public class TransitionImpl implements Transition {

    private static final String MAGELLAN_VIEWBEAN_PACKAGE = "${magellan.viewbeans.packagename}";

    private final Logger logger = getApplicationLogger(
            TransitionImpl.class);

    private final BeanGraph graph;

    @Getter
    private final Collection<String> classnames;

    @Getter
    @Setter
    @Value(MAGELLAN_VIEWBEAN_PACKAGE)
    private String packagename;

    /**
     * default constructor
     *
     */
    public TransitionImpl() {
        super();

        this.graph = new BeanGraphImpl();
        this.classnames = new ArrayList<>();
    }

    @PostConstruct
    public void postConstruct() {

        classnames
                .clear();

        final Set<Class<?>> beans = getAnnotatedClasses(
                MagellanSessionBean.class);
 
        beans
                .forEach((bean) -> {
                    final MagellanSessionBean annotation = bean
                            .getAnnotation(
                                    MagellanSessionBean.class);
                    if (annotation.auto()) {
                        graph
                                .addVertex(
                                        annotation.vertexe());
                        // keep class name for later use
                        classnames
                                .add(bean
                                        .getName());
                    }
                });

        if (graph
                .vertexes()
                .isEmpty()) {
            logger
                    .warn(
                            "Unable to found any vertexe over @MagellanViewBean");
        }
    }

    @Override
    public TransitionImpl addVertex(final String vertexe) {
        graph
                .addVertex(
                        vertexe);

        return this;
    }

    @Override
    public TransitionImpl addVertexes(final List<String> vertexes) {
        graph
                .addVertexes(
                        vertexes);

        return this;
    }

    @Override
    public TransitionImpl addEdge(final String source,
            final String target) {
        graph
                .addEdge(
                        source, target);

        return this;
    }

    @Override
    public TransitionImpl createGraph() {
        graph
                .createGraph();

        return this;
    }

    @Override
    public int traverse(final String start,
            final Map<String, Object> model) {

        return traverse(
                graph.transition(start), model);
    }

    @Override
    public int traverse(final Map<String, Object> model) {

        return traverse(
                graph.transition(), model);
    }

    /**
     * traverse transistion
     *
     * @param vertexes
     * @param model
     * @return
     */
    protected int traverse(@NotNull final List<String> vertexes,
            final Map<String, Object> model) {

        vertexes
                .forEach((vertexe) -> {

                    final BaseBean bean = (BaseBean) model
                            .get(vertexe);

                    if (bean != null) {
                        final BeanGuard beanGuard = bean.getGuard();

                        if (beanGuard != null) {
                            if (hasExpired(
                                    model, vertexe)) {

                                try {
                                    beanGuard
                                            .execute(
                                                    model);

                                } catch (Exception ex) {

                                    final StringBuilder sb = new StringBuilder()
                                            .append("beanGuard.execute() failed for ")
                                            .append(bean.getBeanId());
                                    logger
                                            .error(
                                                    sb.toString(), ex);
                                }

                                bookmark(
                                        model, vertexe);
                            }
                        }
                    }
                });

        return vertexes
                .size();
    }

    @Override
    public List<String> transition() {

        return graph
                .transition();
    }

    @Override
    public TransitionImpl transitionTail(List<String> vertexes) {

        graph
                .transitionTail(
                        vertexes);

        return this;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> transitionTail() {

        return graph
                .transitionTail();
    }

    /**
     * check if vertexe has expired
     *
     * @param model
     * @param target
     * @return
     */
    protected boolean hasExpired(final Map<String, Object> model,
            final String target) {

        final BaseBean targetBean = (BaseBean) model
                .get(target);

        assert (targetBean != null) : "targetBean missing in hasExpired() "
                + target;

        final Set<String> sources = graph
                .incomingVertexesOf(
                        target);

        // check sources of vertexe
        return sources.stream().anyMatch((source) -> {
            final BaseBean sourceBean = (BaseBean) model
                    .get(source);
            assert (sourceBean != null) : "sourceBean missing in hasExpired() "
                    + source;

            return sourceBean.getLastUpdated()
                    > targetBean.getLastUsed(source);
        });
    }

    /**
     * bookmark last refresh of vertexe with current time
     *
     * @param model
     * @param vertexe
     */
    protected void bookmark(final Map<String, Object> model,
            final String vertexe) {

        bookmark(model, vertexe,
                System.currentTimeMillis());
    }

    /**
     * bookmark last refresh of vertexe with timestamp
     *
     * @param model
     * @param vertexe
     * @param timestamp
     */
    protected void bookmark(final Map<String, Object> model,
            final String vertexe, final long timestamp) {

        final BaseBean target = (BaseBean) model
                .get(vertexe);
        assert (target != null) : " target missing";

        final Set<String> edges = graph
                .incomingVertexesOf(
                        vertexe);
        edges
                .forEach(
                        (source) -> {
                            target
                                    .setLastUsed(
                                            source, timestamp);
                        });

    }

    /**
     * shorter form of toString
     *
     * @return
     */
    public String toTag() {

        final StringBuilder sb = new StringBuilder(
                "Transition[");

        if (!transition().isEmpty()) {

            final Iterator<String> it = transition()
                    .listIterator();
            sb
                    .append(
                            it.next());
            while (it.hasNext()) {
                sb
                        .append(", ")
                        .append(
                                it.next());
            }
        }

        sb
                .append("]");

        return sb
                .toString();
    }

    @Override
    public Set<String> vertexes() {
        return graph.vertexes();
    }
}
