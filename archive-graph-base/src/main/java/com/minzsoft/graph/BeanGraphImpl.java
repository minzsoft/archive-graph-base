package com.minzsoft.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

/**
 * class wrapping an direct acycle graph and the transition
 *
 * @author richard
 */
@ToString
@Accessors(chain = true, fluent = true)
public class BeanGraphImpl implements BeanGraph {

    @Getter
    private final List<String> transition;

    private final Graph<String, BeanEdge> directedGraph;

    @Getter
    private List<String> transitionTail = new ArrayList<>();

    /**
     * default constructor
     */
    public BeanGraphImpl() {

        transition = new ArrayList<>();
        directedGraph = new DirectedAcyclicGraph<>(
                BeanEdge.class);
    }

    @Override
    public BeanGraphImpl addEdge(final String source,
            final String target) {

        directedGraph
                .addEdge(
                        source, target);

        return this;
    }

    @Override
    public BeanGraphImpl addVertex(final String vertexe) {

        if (transitionTail
                .contains(vertexe)) {
            throw new IllegalArgumentException(
                    "vertexe already contained in transitionTail '"
                    + vertexe + "'");
        }

        directedGraph
                .addVertex(
                        vertexe);
        
        return this;
    }

    @Override
    public BeanGraphImpl addVertexes(final List<String> vertexes) {

        vertexes
                .forEach(
                        (vertexe) -> {
                            addVertex(vertexe);
                        });
        return this;
    }

    @Override
    public BeanGraphImpl createGraph() {

        // create transition
        final TopologicalOrderIterator it = new TopologicalOrderIterator(
                directedGraph);

        transition
                .clear();
        it
                .forEachRemaining(
                        (item) -> {
                            transition
                                    .add(
                                            (String) item);
                        });
        transition
                .addAll(
                        transitionTail);
        return this;
    }

    @Override
    public List<String> transition(final String vertexe) {

        final int pos = transition
                .indexOf(
                        vertexe);
        if (pos < 0) {
            throw new IllegalArgumentException(
                    "vertexe '" + vertexe + "' not found");
        }

        final List<String> result = new ArrayList<>();
        for (int i = pos + 1; i < transition.size(); i++) {
            result
                    .add(
                            transition.get(i));
        }

        return result;
    }

    @Override
    public BeanGraphImpl transitionTail(final List<String> transitionTail) {

        transitionTail
                .forEach(
                        (vertexe) -> {
                            if (directedGraph.containsVertex(vertexe)) {

                                throw new IllegalArgumentException(
                                        "vertexe already contained in graph '"
                                        + vertexe + "'");
                            }
                        });

        this.transitionTail = transitionTail;

        return this;
    }

    @Override
    public Set<String> incomingVertexesOf(final String vertexe) {

        if (!transitionTail.contains(vertexe)) {

            final Set<String> result = new HashSet<>();

            final Set<BeanEdge> beanEdges = directedGraph
                    .incomingEdgesOf(
                            vertexe);
            beanEdges
                    .forEach(
                            (beanEdge) -> {
                                result
                                        .add(
                                                (String) beanEdge.getSource());
                            });

            return result;
        }

        return directedGraph
                .vertexSet();
    }

    @Override
    public Set<String> vertexes() {

        return directedGraph
                .vertexSet();
    }
}