package com.minzsoft.cache;

import com.minzsoft.CustomSpringBean;
import java.util.HashMap;
import java.util.Map;
import com.minzsoft.guard.BeanGuard;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import com.minzsoft.guard.MagellanSessionBeanGuard;
import static com.minzsoft.utils.AnnotationUtils.getAnnotatedClasses;

/**
 * BeanGuardCache collect all BeanGuard instances found in <br>package</br>
 *
 * @author richard
 */
@ToString
@Validated
@Component
@CustomSpringBean
@Accessors(chain = true, fluent = true)
@PropertySource(value = {"classpath:Graph.properties"})
public class BeanGuardCache {

    private static final String MAGELLAN_GUARDS_PACKAGE = "${magellan.viewguards.packagename}";

    private final Logger logger = LoggerFactory.getLogger(
            BeanGuardCache.class);

    @Setter
    @Value(MAGELLAN_GUARDS_PACKAGE)
    private String packagename;

    @Getter
    private final Map<String, BeanGuard> cache;

    @Setter
    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    /**
     * default constructor
     */
    public BeanGuardCache() {
        super();

        this.cache = new HashMap<>();
    }

    @PostConstruct
    public void postConstruct() {

        cache.clear();

        final Set<Class<?>> annotations = getAnnotatedClasses(
                MagellanSessionBeanGuard.class);
        annotations
                .forEach((var annotation) -> {

                    try {
                        final Constructor constructor = annotation
                                .getConstructor();

                        //create BeanGuard instance over default constructor
                        final BeanGuard beanGuard = (BeanGuard) constructor
                                .newInstance();

                        assert (beanGuard != null) : "BeanGuard constructor failed";

                        autowireCapableBeanFactory
                                .autowireBean(
                                        beanGuard);

                        //get beanId
                        final MagellanSessionBeanGuard bean = annotation
                                .getAnnotation(
                                        MagellanSessionBeanGuard.class);
                        cache
                                .put(
                                        bean.beanId(), beanGuard);

                        final StringBuilder sb = new StringBuilder("Added key ")
                                .append(bean.beanId())
                                .append(" as bean guard instance ")
                                .append(beanGuard.toString());
                        logger
                                .debug(
                                        sb.toString());
                    } catch (Exception ex) {

                        throw new RuntimeException(
                                ex);
                    }
                });
    }

    /**
     * get guard instance of vertexe or null
     *
     * @param vertexe
     * @return guard of vertexe
     */
    @NotNull
    public BeanGuard get(
            @NotEmpty final String vertexe) {

        final BeanGuard guard = cache
                .get(
                        vertexe);

        if (guard == null) {
            logger
                    .debug(
                            "Using default guard for session bean " + vertexe);
        }

        return (guard != null) ? guard
                : new BeanGuard() {
        };
    }

    /**
     * shorter form of toString
     *
     * @return
     */
    public String toTag() {

        final StringBuilder sb = new StringBuilder(
                "BeanGuardCache(");

        if (!cache().isEmpty()) {

            final Iterator<BeanGuard> it = cache()
                    .values()
                    .iterator();
            sb
                    .append(it.next().getClass().getName());
            while (it.hasNext()) {
                sb
                        .append(", ")
                        .append(it.next().getClass().getName());
            }
        }

        sb
                .append(")");

        return sb
                .toString();
    }
}
