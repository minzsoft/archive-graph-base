package com.minzsoft.guard.test;

import com.minzsoft.bean.BaseBean;
import com.minzsoft.annotation.MagellanSessionBean;

/**
 *
 * @author richard
 */
@MagellanSessionBean(vertexe = "testBean2")
public class TestBean2 extends BaseBean {

    public TestBean2(String beanId) {
        super(beanId);
    }
}
