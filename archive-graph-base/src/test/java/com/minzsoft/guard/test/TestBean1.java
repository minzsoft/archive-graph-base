package com.minzsoft.guard.test;

import com.minzsoft.bean.BaseBean;
import com.minzsoft.annotation.MagellanSessionBean;

/**
 *
 * @author richard
 */
@MagellanSessionBean(vertexe = "testBean1")
public class TestBean1 extends BaseBean {

    public TestBean1(String beanId) {
        super(beanId);
    }
}
