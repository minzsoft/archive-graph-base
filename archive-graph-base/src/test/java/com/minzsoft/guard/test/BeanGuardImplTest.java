/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minzsoft.guard.test;

import com.minzsoft.guard.BeanGuard;
import java.util.Map;
import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author richard
 */
public class BeanGuardImplTest {
    
    public static class AnyObject {
        
    }

    /**
     * Test of execute method, of class BeanGuard.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testExecuteOkay()
            throws Exception {

        final BeanGuard guard = getBeanImplCtrl();

        new Expectations(guard) {
            {
                guard.refresh((Map<String, Object>) any);
            }
        };

        guard.execute(Map.of());

        new Verifications() {
            {
                guard.refresh((Map<String, Object>) any);
                times = 1;
            }
        };
    }

    /**
     * Test of refreshBean method, of class BeanGuard.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = RuntimeException.class)
    public void testExecuteFailed()
            throws Exception {

        final BeanGuard guard = getBeanImplCtrl();

        new Expectations(guard) {
            {
                guard.refresh((Map<String, Object>) any);
                result = new NullPointerException();
            }
        };

        guard.execute(Map.of());
    }

    /**
     * Test of getSessionBeans method, of class BaseController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetSessionBean1()
            throws Exception {
        
        final AnyObject object = new AnyObject();
        
        final Map<String, Object> map = Map
                .of("this", object);

        final Object bean = getBeanImplCtrl()
                .getSessionBean(map, "this");

        assertEquals(bean, object);
    }

    /**
     * Test of refresh method, of class BaseController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRefresh()
            throws Exception {

        getBeanImplCtrl().refresh(
                Map.of());
    }

    /**
     *
     * @return
     */
    private BeanGuard getBeanImplCtrl() {

        return new BeanGuard() {
        };
    }
}
