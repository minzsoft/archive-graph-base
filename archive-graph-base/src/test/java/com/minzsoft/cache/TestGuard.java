package com.minzsoft.cache;

import com.minzsoft.guard.BeanGuard;
import com.minzsoft.guard.MagellanSessionBeanGuard;

/**
 *
 * @author richard
 */
@MagellanSessionBeanGuard(beanId = "testBean")
public class TestGuard implements BeanGuard {

    /**
     * default constructor
     */
    public TestGuard() {
        super();
    }
 }
