package com.minzsoft.cache;

import mockit.Injectable;
import org.junit.Test;
import static org.junit.Assert.*;
import com.minzsoft.guard.BeanGuard;
import com.minzsoft.test.CustomMockFactory;
import java.util.HashSet;
import java.util.Set;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Verifications;
import org.junit.Before;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

/**
 *
 * @author richard
 */
public class BeanGuardCacheTest {

    @Injectable
    private Logger mockedLogger;

    @Injectable
    private AutowireCapableBeanFactory mockedAutowireCapableBeanFactory;

    @Before
    public void before()
            throws Exception {

        new Expectations() {
            {
                mockedAutowireCapableBeanFactory
                        .autowireBean(
                                any);
                minTimes = 0;
            }
        };
    }

    /**
     * Test of BeanGuardCache constructor, of class BeanGuardCache.
     */
    @Test
    public void testBeanGuardCache() {

        final BeanGuardCache cache = new BeanGuardCache();

        assertNotNull(
                cache);

        assertTrue(
                cache.toString().startsWith("BeanGuardCache("));
    }

    /**
     * Test of postConstruct method, of class BeanGuardCache.
     */
    @Test
    public void testPostConstruct_okay() {
        
        CustomMockFactory
                .mockUpLoggerFactory_getLogger(
                        mockedLogger);

        new MockUp<Reflections>() {

            @Mock
            public void $init(String path) {
            }

            @Mock
            public Set<Class<?>> getTypesAnnotatedWith(Class clazz) {

                final Set result = new HashSet<>();
                result
                        .add(
                                TestGuard.class);
                return result;
            }
        };

        final BeanGuardCache cache = getBeanGuardCache();
        cache
                .postConstruct();

        assertNotNull(
                cache);

        assertTrue(
                !cache.cache().isEmpty());
               
        new Verifications() {
            {
                mockedLogger
                        .info(
                                anyString);
                times = 0;
            }
        };
    }

    /**
     * Test of postConstruct method, of class BeanGuardCache.
     */
    @Test(expected = RuntimeException.class)
    public void testPostConstruct_RuntimeException() {

        new MockUp<Reflections>() {

            @Mock
            public void $init(String path) {
            }

            @Mock
            public Set<Class<?>> getTypesAnnotatedWith(Class clazz) {

                final Set result = new HashSet<>();
                result
                        .add(
                                TestGuard.class);

                return result;
            }
        };

        new Expectations() {
            {
                mockedAutowireCapableBeanFactory
                        .autowireBean(
                                any);
                result = new NullPointerException();
            }
        };

        final BeanGuardCache cache = getBeanGuardCache();

        cache
                .postConstruct();
    }
    
    /**
     * Test of get method, of class BeanGuardCache.
     */
    @Test
    public void testGet_okay() {
        
        CustomMockFactory
                .mockUpLoggerFactory_getLogger(
                        mockedLogger);

        new MockUp<Reflections>() {

            @Mock
            public void $init(String path) {
            }

            @Mock
            public Set<Class<?>> getTypesAnnotatedWith(Class clazz) {

                final Set result = new HashSet<>();
                result
                        .add(
                                TestGuard.class);
                return result;
            }
        };

        final BeanGuardCache cache = getBeanGuardCache();
        cache
                .postConstruct();
        
        final BeanGuard guard = cache
                .get(
                        "testBean");
        
        assertNotNull(
                guard);
               
        new Verifications() {
            {
                mockedLogger
                        .warn(
                                anyString);
                times = 0;
            }
        };
    }

    /**
     * Test of get method, of class BeanGuardCache.
     */
    @Test
    public void testGet_failed() {
        
        CustomMockFactory
                .mockUpLoggerFactory_getLogger(
                        mockedLogger);

        new MockUp<Reflections>() {

            @Mock
            public void $init(String path) {
            }

            @Mock
            public Set<Class<?>> getTypesAnnotatedWith(Class clazz) {

                final Set result = new HashSet<>();
                result
                        .add(
                                TestGuard.class);
                return result;
            }
        };

        final BeanGuardCache cache = getBeanGuardCache();
        cache
                .postConstruct();
        
        final BeanGuard guard = cache
                .get(
                        "testBean1");
        
        assertNotNull(
                guard);
               
        new Verifications() {
            {
                mockedLogger
                        .debug(
                                "Using default guard for session bean testBean1");
                times = 1;
            }
        };
    }

    /**
     * get test instance of BeanGuardCache
     *
     * @return
     */
    private BeanGuardCache getBeanGuardCache() {

        final BeanGuardCache cache = new BeanGuardCache()
                .packagename("com.minzsoft.cache")
                .autowireCapableBeanFactory(
                        mockedAutowireCapableBeanFactory);

        return cache;
    }
}
