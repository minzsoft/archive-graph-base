package com.minzsoft.cache;

import com.minzsoft.bean.BaseBean;
import com.minzsoft.annotation.MagellanSessionBean;

/**
 *
 * @author richard
 */
@MagellanSessionBean(vertexe = TestBean.BEANID)
public class TestBean extends BaseBean {
    
    public static final String BEANID = "testBean";

    public TestBean() {
        super(BEANID);
    }

    @Override
    public String getBeanId() {
        return BEANID;
    }
}
