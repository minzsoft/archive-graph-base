package com.minzsoft.graph;

import com.minzsoft.cache.BeanGuardCache;
import com.minzsoft.cache.TestGuard;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mockit.Expectations;
import mockit.Injectable;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author richard
 */
public class TransitionMgrBaseTest {

    public class TransitionMgrBaseImpl extends TransitionMgrBase {

        @Override
        public Map<String, Object> createSession()
                throws Exception {
            return Map.of();
        }
    }

    @Injectable
    private Transition mockedTransition;

    @Injectable
    private BeanGuardCache mockedBeanGuardCache;

    /**
     * Test of createAutoBeans method, of class TransitionMgrBase.
     *
     * @throws java.lang.Exception
     */
    @Before
    public void before()
            throws Exception {
        
        final List<String> classnames = new ArrayList<>();
        classnames
                .add("com.minzsoft.cache.TestBean");

        new Expectations() {
            {
                mockedTransition
                        .classnames();
                result = classnames;
                minTimes = 0;

                mockedBeanGuardCache
                        .get(
                                anyString);
                result = new TestGuard();
                minTimes = 0;
            }
        };
    }

    /**
     * Test of createAutoBeans method, of class TransitionMgrBase.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateAutoBeans_okay()
            throws Exception {

        final TransitionMgrBase transitionMgr = getTransitionMgrBase();

        final Map<String, Object> result = transitionMgr
                .createAutoBeans();
        
        assertEquals(result.size(),1);
    }
    
    /**
     * Test of createAutoBeans method, of class TransitionMgrBase.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = RuntimeException.class)
    public void testCreateAutoBeans_fail()
            throws Exception {
                
        final List<String> classnames = new ArrayList<>();
        classnames
                .add("com.minzsoft.cache.unknown");

        new Expectations() {
            {
                mockedTransition
                        .classnames();
                result = classnames;
            }
        };

        final TransitionMgrBase transitionMgr = getTransitionMgrBase();

        transitionMgr
                .createAutoBeans();
    }

    /**
     * create test instance of class TransitionMgrBase.
     */
    private TransitionMgrBase getTransitionMgrBase() {

        final TransitionMgrBase transitionMgr = new TransitionMgrBaseImpl()
                .transition(mockedTransition)
                .beanGuardCache(mockedBeanGuardCache);

        return transitionMgr;
    }

}
