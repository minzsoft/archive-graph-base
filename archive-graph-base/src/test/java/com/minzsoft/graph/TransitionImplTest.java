/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minzsoft.graph;

import com.minzsoft.bean.BaseBean;
import com.minzsoft.guard.BeanGuard;
import com.minzsoft.guard.test.TestBean1;
import com.minzsoft.guard.test.TestBean2;
import com.minzsoft.test.CustomMockFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import mockit.Injectable;
import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import org.junit.Before;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Ignore;
import org.slf4j.Logger;

/**
 *
 * @author richard
 */
public class TransitionImplTest {

    private final static String PACKAGE_NAME = "com.minzsoft.guard.test";

    private final static String BEANID = "beanId";

    @Injectable
    private Logger mockedLogger;

    @Injectable
    private BeanGuard mockedGuard1;

    @Injectable
    private BeanGuard mockedGuard2;

    @Before
    public void before() throws Exception {

        new Expectations() {
            {
                mockedGuard1
                        .refresh(
                                (Map<String, Object>) any);
                minTimes = 0;

                mockedGuard2
                        .refresh(
                                (Map<String, Object>) any);
                minTimes = 0;
            }
        };
    }

    /**
     * Test of Transition constructor, of class TransitionMgr.
     */
    @Test
    public void testTransitionImpl() {

        final Map<String, Object> map = new HashMap<>();
        map.put("testBean1", new TestBean1("testBean1"));
        map.put("testBean2", new TestBean2("testBean2"));

        final TransitionImpl transition = getTransitionImpl(true)
                .addEdge(
                        "testBean1", "testBean2");

        transition
                .createGraph();

        assertNotNull(
                transition);
    }

    /**
     * Test of toString method, of class TransitionMgr.
     */
    @Test
    public void testToString() {

        final TransitionImpl transition = getTransitionImpl()
                .addVertex("a")
                .addVertex("b")
                .addEdge("a", "b")
                .createGraph();

        assertTrue(
                transition.toString().startsWith("TransitionImpl("));
    }

    /**
     * Test of addVertexes method, of class TransitionMgr.
     */
    @Test
    public void testAddVertexes() {

        final TransitionImpl transitionImpl = getTransitionImpl();

        transitionImpl
                .addVertexes(
                        Arrays
                                .asList("test1"));
    }

    /**
     * Test of traverse method, of class TransitionMgr.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testTraverse1_IllegalArgumentException() {

        final TransitionImpl transitionImpl = getTransitionImpl();

        transitionImpl
                .createGraph();

        transitionImpl
                .traverse(
                        "unkown", Map.of());
    }

    /**
     * Test of traverse method, of class TransitionMgr.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTraverse1_okay() throws Exception {

        final long timestamp = System.currentTimeMillis();

        // create source
        final BaseBean baseBean1 = new BaseBean("test1");
        baseBean1.setGuard(mockedGuard1);
        baseBean1.setLastUpdated(timestamp);

        // create target
        final BaseBean baseBean2 = new BaseBean("test2");
        baseBean2.setGuard(mockedGuard2);
        baseBean2.setLastUsed("test1", timestamp - 1000);
        baseBean2.setLastUpdated(timestamp);

        final Map<String, Object> map = new HashMap<>();
        map.put("test1", baseBean1);
        map.put("test2", baseBean2);

        final TransitionImpl transitionImpl = getTransitionImpl(
                false);

        new Expectations(transitionImpl) {
            {
                transitionImpl
                        .bookmark(
                                (Map<String, Object>) any, anyString);
            }
        };

        transitionImpl
                .createGraph();

        final int result = transitionImpl
                .traverse(
                        baseBean1.getBeanId(), map);

        assertEquals(result, 1);

        new Verifications() {
            {
                transitionImpl
                        .bookmark(
                                (Map<String, Object>) any, anyString);
                times = 1;
            }
        };
    }

    /**
     * Test of traverse method, of class TransitionMgr.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTraverse2_okay() throws Exception {

        final long timestamp = System.currentTimeMillis();

        // create source
        final BaseBean baseBean1 = new BaseBean("test1");
        baseBean1.setGuard(mockedGuard1);
        baseBean1.setLastUpdated(timestamp);

        // create target
        final BaseBean baseBean2 = new BaseBean("test2");
        baseBean2.setGuard(mockedGuard2);
        baseBean2.setLastUsed("test1", timestamp - 1000);
        baseBean2.setLastUpdated(timestamp);

        final Map<String, Object> map = new HashMap<>();
        map.put("test1", baseBean1);
        map.put("test2", baseBean2);

        final TransitionImpl transitionImpl = getTransitionImpl(
                false);

        new Expectations(transitionImpl) {
            {
                transitionImpl
                        .bookmark(
                                (Map<String, Object>) any, anyString);
            }
        };

        transitionImpl
                .createGraph();

        final int result = transitionImpl
                .traverse(
                        map);

        assertEquals(result, 2);

        new Verifications() {
            {
                transitionImpl
                        .bookmark(
                                (Map<String, Object>) any, anyString);
                times = 1;
            }
        };
    }

    /**
     * Test of hasExpired method, of class TransitionMgr.
     */
    @Test
    public void testHasExpiredTrue() {

        final long timestamp = System.currentTimeMillis();

        // create source
        final BaseBean baseBean1 = new BaseBean(BEANID);
        baseBean1.setLastUpdated(timestamp);

        // create target
        final BaseBean baseBean2 = new BaseBean(BEANID);
        baseBean2.setLastUsed("test1", timestamp - 1000);
        baseBean2.setLastUpdated(timestamp);

        final Map<String, Object> map = new HashMap<>();
        map.put("test1", baseBean1);
        map.put("test2", baseBean2);

        final TransitionImpl mgr = getTransitionImpl();
        mgr.createGraph();

        final boolean result = mgr.hasExpired(map, "test2");

        assertTrue(result);
    }

    /**
     * Test of hasExpired method, of class TransitionMgr.
     */
    @Test
    public void testHasExpiredFalse() {

        final long timestamp = System.currentTimeMillis();

        // create source
        final BaseBean baseBean1 = new BaseBean(BEANID);
        baseBean1.setLastUpdated(timestamp);

        // create target
        final BaseBean baseBean2 = new BaseBean(BEANID);
        baseBean2.setLastUsed("test1", timestamp);
        baseBean2.setLastUpdated(timestamp);

        final Map<String, Object> map = new HashMap<>();
        map.put("test1", baseBean1);
        map.put("test2", baseBean2);

        final TransitionImpl mgr = getTransitionImpl();
        mgr.createGraph();

        final boolean result = mgr.hasExpired(map, "test2");

        assertTrue(!result);
    }

    /**
     * Test of bookmark method, of class TransitionMgr.
     */
    @Test
    public void testBookmark1() {

        final long timestamp = System.currentTimeMillis()
                - 1000;

        // create source
        final BaseBean baseBean1 = new BaseBean(BEANID);
        baseBean1.setLastUpdated(timestamp);

        // create target
        final BaseBean baseBean2 = new BaseBean(BEANID);
        baseBean2.setLastUpdated(timestamp);
        baseBean2.setLastUsed("test1", timestamp);

        final Map<String, Object> map = new HashMap<>();
        map.put("test1", baseBean1);
        map.put("test2", baseBean2);

        final TransitionImpl mgr = getTransitionImpl();
        mgr.createGraph();
        mgr.bookmark(map, "test2");

        final BaseBean target = (BaseBean) map.get("test2");

        assertTrue(target.getLastUsed("test1") > timestamp);
    }

    /**
     * Test of packageName methode, of class TransitionMgr.
     */
    @Test
    public void testPackageName() {

        final TransitionImpl mgr = getTransitionImpl();

        assertEquals(
                mgr.packagename(), PACKAGE_NAME);
    }

    /**
     * Test of transition methode, of class TransitionMgr.
     */
    @Test
    public void testTransition() {

        final TransitionImpl mgr = getTransitionImpl(false)
                .createGraph();

        assertEquals(mgr.transition().size(), 2);
        assertEquals(mgr.transition().toString(),
                "[test1, test2]");
    }

    /**
     * Test of transitionTail methode, of class TransitionMgr.
     */
    @Test
    public void testTransitionTail() {

        final List<String> vertexes = Arrays.asList("beanId");

        final TransitionImpl mgr = getTransitionImpl()
                .transitionTail(vertexes);

        assertEquals(
                mgr.transitionTail(), vertexes);
    }

    /**
     * Test of transitionTail methode, of class TransitionMgr.
     */
    @Test
    public void testPostConstruct1() {

        CustomMockFactory
                .mockUpLoggerFactory_getLogger(
                        mockedLogger);

        final TransitionImpl transition = new TransitionImpl()
                .packagename(
                        "temp");

        transition
                .postConstruct();

        new Verifications() {
            {
                mockedLogger
                        .info(
                                anyString);
                times = 0;

                mockedLogger
                        .warn(
                                anyString);
                times = 0;
            }
        };
    }

    /**
     * Test of classnames methode, of class TransitionMgr.
     */
    @Test
    @Ignore
    public void testClassnames() {

        final TransitionImpl transition = new TransitionImpl()
                .packagename(
                        "com.minzsoft.guard");

        transition
                .postConstruct();

        final Collection<String> result = transition
                .classnames();

        assertEquals(
                result, List.of("com.minzsoft.guard.test.TestBean1", "com.minzsoft.guard.test.TestBean2"));
    }

    /**
     *
     * @return
     */
    private TransitionImpl getTransitionImpl() {

        return getTransitionImpl(
                true);
    }

    /**
     *
     * @return
     */
    private TransitionImpl getTransitionImpl(final boolean postConstruct) {

        final TransitionImpl transition = new TransitionImpl()
                .packagename(PACKAGE_NAME)
                .addVertex("test1")
                .addVertex("test2")
                .addEdge(
                        "test1", "test2");

        if (postConstruct) {
            transition
                    .postConstruct();
        }

        return transition;
    }
}
