/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minzsoft.graph;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author richard
 */
public class BeanEdgeTest {

    /**
     * Test of getSource method, of class BeanEdge.
     */
    @Test
    public void testGetSource() {

        final BeanEdge edge = new BeanEdge();

        assertThat(edge.getSource(),
                CoreMatchers.nullValue());
    }

    /**
     * Test of getTarget method, of class BeanEdge.
     */
    @Test
    public void testGetTarget() {

        final BeanEdge edge = new BeanEdge();

        assertThat(edge.getTarget(),
                CoreMatchers.nullValue());
    }
}
