/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minzsoft.graph;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author richard
 */
public class BeanGraphImplTest {

    /**
     * Test of constructor method, of class BeanGraphImpl.
     */
    @Test
    public void testBeanGraphImpl() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();

        assertNotNull(
                beanGraph);
    }

    /**
     * Test of toString method, of class BeanGraphImpl.
     */
    @Test
    public void testToString() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();

        assertTrue(
                beanGraph.toString().startsWith("BeanGraphImpl("));
    }

    /**
     * Test of addEdge method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition1() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c", "e", "f", "h"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("e", "f");
        beanGraph.addEdge("f", "h");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, e, c, f, h]");
    }

    /**
     * Test of addEdge method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition2() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c", "e", "f", "h"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("e", "f");
        beanGraph.addEdge("e", "h");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, e, c, f, h]");
    }

    /**
     * Test of addEdge method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition3() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("a", "c");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, c]");
    }

    /**
     * Test of addEdge method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition4() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "b", "c", "d"));

        beanGraph.addEdge("a", "d");
        beanGraph.addEdge("b", "d");
        beanGraph.addEdge("c", "d");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, b, c, d]");
    }

    /**
     * Test of initial method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition5() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "b", "c"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("b", "c");
        beanGraph.addEdge("b", "a");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[b, a, c]");
    }

    /**
     * Test of initial method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateTransition6() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("c", "a");

        beanGraph.createGraph();
    }

    /**
     * Test of initial method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateTransition7() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a"));

        beanGraph.addEdge("a", "a");

        beanGraph.createGraph();
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition8() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertex("a");

        beanGraph.createGraph();

        assertTrue(beanGraph.transition().size() > 0);
    }

    /**
     * Test of initial method, of class BeanGraphImpl.
     */
    @Test
    public void testCreateTransition9() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertex("a0");
        for (int i = 0; i < 100; i++) {
            beanGraph.addVertex("a" + Integer.toString(i + 1));

            beanGraph.addEdge("a" + Integer.toString(i),
                    "a" + Integer.toString(i + 1));
        }

        final long begin = System.currentTimeMillis();

        beanGraph.createGraph();

        final long finish = System.currentTimeMillis();

        System.out.println("------------------------------------");
        System.out.println("Neeed time for createTransition "
                + (finish - begin) + " msec");
        System.out.println("------------------------------------");
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetTransition1() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c", "e", "f", "h"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("e", "f");
        beanGraph.addEdge("f", "h");

        beanGraph.createGraph();

        beanGraph.transition("x");
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test
    public void testGetTransition2() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c", "e", "f", "h"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("e", "f");
        beanGraph.addEdge("f", "h");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition("f").toString(), "[h]");
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test
    public void testGetTransition3() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c", "e", "f", "h"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("c", "e");
        beanGraph.addEdge("e", "f");
        beanGraph.addEdge("f", "h");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, c, e, f, h]");
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test
    public void testGetTransition4() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c"));

        beanGraph.addEdge("a", "c");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.transition().toString(), "[a, c]");
    }

    /**
     * Test of addVertex method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddVertex1() {

        final String A = "a";

        final List<String> vertexes = Arrays.asList(A);

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph
                .transitionTail(
                        vertexes);

        beanGraph
                .addVertex(
                        A);
    }

    /**
     * Test of addVertexes method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddVertex2() {

        final List<String> vertexes = Arrays.asList("a");

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.transitionTail(vertexes);

        beanGraph.addVertexes(vertexes);
    }

    /**
     * Test of incomingVertexesOf method, of class BeanGraphImpl.
     */
    @Test
    public void testGetIncomingVertexesOf1() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "b", "c"));

        beanGraph.addEdge("a", "c");
        beanGraph.addEdge("b", "c");

        beanGraph.createGraph();

        assertEquals(
                beanGraph.incomingVertexesOf("c").toString(), "[a, b]");
    }

    /**
     * Test of incomingVertexesOf method, of class BeanGraphImpl.
     */
    @Test
    public void testGetIncomingVertexesOf2() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "c"));

        beanGraph.addEdge("a", "c");

        beanGraph.createGraph();

        assertEquals(beanGraph.incomingVertexesOf("a").toString(),
                "[]");
    }

    /**
     * Test of incomingVertexesOf method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetIncomingVertexesOf3() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();

        beanGraph.createGraph();

        beanGraph.incomingVertexesOf("a");
    }

    /**
     * Test of incomingVertexesOf method, of class BeanGraphImpl.
     */
    @Test
    public void testGetIncomingVertexesOf4() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays
                .asList("a", "b", "c"));

        beanGraph.addEdge("a", "b");
        beanGraph.addEdge("b", "c");

        beanGraph.createGraph();

        assertEquals(beanGraph.incomingVertexesOf("c").toString(),
                "[b]");
    }

    /**
     * Test of incomingVertexesOf method, of class BeanGraphImpl.
     */
    @Test
    public void testGetIncomingVertexesOf5() {

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays.asList("a", "b", "c"));
        beanGraph.transitionTail(Arrays.asList("d"));

        beanGraph.addEdge("a", "b");
        beanGraph.addEdge("b", "c");

        beanGraph.createGraph();

        assertEquals(beanGraph.incomingVertexesOf("d").toString(),
                "[a, b, c]");
    }

    /**
     * Test of transition methode, of class BeanGraphImpl.
     */
    @Test
    public void testSetGetTransitionTail1() {

        final List<String> transitionTail = Arrays.asList("b");

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays.asList("a"));
        beanGraph.transitionTail(transitionTail);

        beanGraph.createGraph();

        assertEquals(beanGraph.transition().toString(),
                "[a, b]");
        assertEquals(beanGraph.transitionTail(),
                transitionTail);
    }

    /**
     * Test of transition methode, of class BeanGraphImpl.
     */
    @Test
    public void testSetTransitionTail1() {

        final String A = "a";

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays.asList(A));

        beanGraph.createGraph();

        assertEquals(beanGraph.transition().toString(),
                "[a]");
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetTransitionTail3() {

        final String A = "a";

        final BeanGraphImpl beanGraph = new BeanGraphImpl();
        beanGraph.addVertexes(Arrays.asList(A));

        beanGraph.transitionTail(Arrays.asList(A));
    }

    /**
     * Test of transition method, of class BeanGraphImpl.
     */
    @Test
    public void testSetTransitionTail4() {

        final String A = "a";
        final String B = "b";
        final String C = "c";

        final BeanGraphImpl beanGraph = new BeanGraphImpl()
                .addVertexes(
                        Arrays.asList(A));

        beanGraph
                .transitionTail(
                        Arrays.asList(B, C));

        beanGraph.createGraph();

        assertEquals(beanGraph.transition().toString(),
                "[a, b, c]");
    }
    
    /**
     * Test of vertexes method, of class BeanGraphImpl.
     */
    @Test
    public void testVertexes1() {

        final String a = "a";

        final BeanGraphImpl beanGraph = new BeanGraphImpl()
                .addVertexes(
                        Arrays.asList(a));

        final Set<String> result = beanGraph
                .vertexes();

        assertEquals(
                result.size(),1);
    }
}
